// Code generated by Prisma (prisma@1.34.6). DO NOT EDIT.
  // Please don't change this file manually but run `prisma generate` to update it.
  // For more information, please read the docs: https://www.prisma.io/docs/prisma-client/

export const typeDefs = /* GraphQL */ `type AggregateTest {
  count: Int!
}

type AggregateUser {
  count: Int!
}

type BatchPayload {
  count: Long!
}

scalar Long

type Mutation {
  createTest(data: TestCreateInput!): Test!
  updateTest(data: TestUpdateInput!, where: TestWhereUniqueInput!): Test
  updateManyTests(data: TestUpdateManyMutationInput!, where: TestWhereInput): BatchPayload!
  upsertTest(where: TestWhereUniqueInput!, create: TestCreateInput!, update: TestUpdateInput!): Test!
  deleteTest(where: TestWhereUniqueInput!): Test
  deleteManyTests(where: TestWhereInput): BatchPayload!
  createUser(data: UserCreateInput!): User!
  updateUser(data: UserUpdateInput!, where: UserWhereUniqueInput!): User
  updateManyUsers(data: UserUpdateManyMutationInput!, where: UserWhereInput): BatchPayload!
  upsertUser(where: UserWhereUniqueInput!, create: UserCreateInput!, update: UserUpdateInput!): User!
  deleteUser(where: UserWhereUniqueInput!): User
  deleteManyUsers(where: UserWhereInput): BatchPayload!
}

enum MutationType {
  CREATED
  UPDATED
  DELETED
}

interface Node {
  id: ID!
}

type PageInfo {
  hasNextPage: Boolean!
  hasPreviousPage: Boolean!
  startCursor: String
  endCursor: String
}

type Query {
  test(where: TestWhereUniqueInput!): Test
  tests(where: TestWhereInput, orderBy: TestOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Test]!
  testsConnection(where: TestWhereInput, orderBy: TestOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): TestConnection!
  user(where: UserWhereUniqueInput!): User
  users(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [User]!
  usersConnection(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): UserConnection!
  node(id: ID!): Node
}

type Subscription {
  test(where: TestSubscriptionWhereInput): TestSubscriptionPayload
  user(where: UserSubscriptionWhereInput): UserSubscriptionPayload
}

type Test {
  testId: ID!
  TestName: String
}

type TestConnection {
  pageInfo: PageInfo!
  edges: [TestEdge]!
  aggregate: AggregateTest!
}

input TestCreateInput {
  testId: ID
  TestName: String
}

type TestEdge {
  node: Test!
  cursor: String!
}

enum TestOrderByInput {
  testId_ASC
  testId_DESC
  TestName_ASC
  TestName_DESC
}

type TestPreviousValues {
  testId: ID!
  TestName: String
}

type TestSubscriptionPayload {
  mutation: MutationType!
  node: Test
  updatedFields: [String!]
  previousValues: TestPreviousValues
}

input TestSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: TestWhereInput
  AND: [TestSubscriptionWhereInput!]
}

input TestUpdateInput {
  TestName: String
}

input TestUpdateManyMutationInput {
  TestName: String
}

input TestWhereInput {
  testId: ID
  testId_not: ID
  testId_in: [ID!]
  testId_not_in: [ID!]
  testId_lt: ID
  testId_lte: ID
  testId_gt: ID
  testId_gte: ID
  testId_contains: ID
  testId_not_contains: ID
  testId_starts_with: ID
  testId_not_starts_with: ID
  testId_ends_with: ID
  testId_not_ends_with: ID
  TestName: String
  TestName_not: String
  TestName_in: [String!]
  TestName_not_in: [String!]
  TestName_lt: String
  TestName_lte: String
  TestName_gt: String
  TestName_gte: String
  TestName_contains: String
  TestName_not_contains: String
  TestName_starts_with: String
  TestName_not_starts_with: String
  TestName_ends_with: String
  TestName_not_ends_with: String
  AND: [TestWhereInput!]
}

input TestWhereUniqueInput {
  testId: ID
}

type User {
  UserId: ID!
  FirstName: String!
  LastName: String!
  Email: String
  CompanyName: String
  Role: UserRoles
}

type UserConnection {
  pageInfo: PageInfo!
  edges: [UserEdge]!
  aggregate: AggregateUser!
}

input UserCreateInput {
  UserId: ID
  FirstName: String!
  LastName: String!
  Email: String
  CompanyName: String
  Role: UserRoles
}

type UserEdge {
  node: User!
  cursor: String!
}

enum UserOrderByInput {
  UserId_ASC
  UserId_DESC
  FirstName_ASC
  FirstName_DESC
  LastName_ASC
  LastName_DESC
  Email_ASC
  Email_DESC
  CompanyName_ASC
  CompanyName_DESC
  Role_ASC
  Role_DESC
}

type UserPreviousValues {
  UserId: ID!
  FirstName: String!
  LastName: String!
  Email: String
  CompanyName: String
  Role: UserRoles
}

enum UserRoles {
  SUPERADMIN
  ADMIN
}

type UserSubscriptionPayload {
  mutation: MutationType!
  node: User
  updatedFields: [String!]
  previousValues: UserPreviousValues
}

input UserSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: UserWhereInput
  AND: [UserSubscriptionWhereInput!]
}

input UserUpdateInput {
  FirstName: String
  LastName: String
  Email: String
  CompanyName: String
  Role: UserRoles
}

input UserUpdateManyMutationInput {
  FirstName: String
  LastName: String
  Email: String
  CompanyName: String
  Role: UserRoles
}

input UserWhereInput {
  UserId: ID
  UserId_not: ID
  UserId_in: [ID!]
  UserId_not_in: [ID!]
  UserId_lt: ID
  UserId_lte: ID
  UserId_gt: ID
  UserId_gte: ID
  UserId_contains: ID
  UserId_not_contains: ID
  UserId_starts_with: ID
  UserId_not_starts_with: ID
  UserId_ends_with: ID
  UserId_not_ends_with: ID
  FirstName: String
  FirstName_not: String
  FirstName_in: [String!]
  FirstName_not_in: [String!]
  FirstName_lt: String
  FirstName_lte: String
  FirstName_gt: String
  FirstName_gte: String
  FirstName_contains: String
  FirstName_not_contains: String
  FirstName_starts_with: String
  FirstName_not_starts_with: String
  FirstName_ends_with: String
  FirstName_not_ends_with: String
  LastName: String
  LastName_not: String
  LastName_in: [String!]
  LastName_not_in: [String!]
  LastName_lt: String
  LastName_lte: String
  LastName_gt: String
  LastName_gte: String
  LastName_contains: String
  LastName_not_contains: String
  LastName_starts_with: String
  LastName_not_starts_with: String
  LastName_ends_with: String
  LastName_not_ends_with: String
  Email: String
  Email_not: String
  Email_in: [String!]
  Email_not_in: [String!]
  Email_lt: String
  Email_lte: String
  Email_gt: String
  Email_gte: String
  Email_contains: String
  Email_not_contains: String
  Email_starts_with: String
  Email_not_starts_with: String
  Email_ends_with: String
  Email_not_ends_with: String
  CompanyName: String
  CompanyName_not: String
  CompanyName_in: [String!]
  CompanyName_not_in: [String!]
  CompanyName_lt: String
  CompanyName_lte: String
  CompanyName_gt: String
  CompanyName_gte: String
  CompanyName_contains: String
  CompanyName_not_contains: String
  CompanyName_starts_with: String
  CompanyName_not_starts_with: String
  CompanyName_ends_with: String
  CompanyName_not_ends_with: String
  Role: UserRoles
  Role_not: UserRoles
  Role_in: [UserRoles!]
  Role_not_in: [UserRoles!]
  AND: [UserWhereInput!]
}

input UserWhereUniqueInput {
  UserId: ID
}
`